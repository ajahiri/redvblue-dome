package me.arianjahiri.DomeWars;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.domedd.developerapi.messagebuilder.TitleBuilder;

public class DomeWars extends JavaPlugin {
	// Fired when plugin is first enabled
    @Override
	public void onEnable() {
		Bukkit.getServer().getLogger().info("DomeWars by Arian Jahiri has loaded successfully!"); //Log to console
	}
    
    // Fired when plugin is disabled
    @Override
	public void onDisable() {
		Bukkit.getServer().getLogger().info("DomeWars is disabed!"); //Log to console
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		if (!(sender instanceof Player)) { //If sender is not an instance of Player (not a player)
			sender.sendMessage("The console ran the test command!");
			return true; //End here is not a player
		}
		if (!sender.hasPermission("domewars.start")) {
			sender.sendMessage(ChatColor.RED + "You are not permitted to do this!");
			return true;
		}
		
		Player player = (Player) sender;
		
		if (cmd.getName().equalsIgnoreCase("domewars")) { //If command "./domewars"
			if (args.length == 0) {
				player.sendMessage(ChatColor.GOLD + "You ran the test command! Well done!");
				return true;
			} 
			
			if (args[0].equalsIgnoreCase("start")) {
				Collection<? extends Player> onlinePlayers = Bukkit.getServer().getOnlinePlayers();
				for (Player p : onlinePlayers) {
					TitleBuilder tb = new TitleBuilder(ChatColor.RED + "DomeWars", ChatColor.AQUA + "Starting DomeWars match now!");
					tb.setTimings(10, 60, 10, 10, 60, 10);
					tb.send(p);
					p.sendMessage(ChatColor.GOLD + "Start DOMEWARS!");
				}
				return true;
			}
			
		}
		return true;
	
	} 
}

